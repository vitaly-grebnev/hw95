FROM centos:7

RUN yum update -y \
    && yum -y install wget make gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel \
    && wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz \
    && tar xzf Python-3.7.11.tgz \
    && cd Python-3.7.11 \
    && ./configure --enable-optimizations \
    && make altinstall \
    && cd .. \
    && rm Python-3.7.11.tgz \
    && rm -rf Python-3.7.11 \
    && ln -sfn /usr/local/bin/python3.7 /usr/bin/python3.7 \
    && ln -sfn /usr/local/bin/pip3.7 /usr/bin/pip3.7 \
    && python3.7 -m pip install --upgrade pip \
    && pip3 install flask flask_restful flask_jsonpify \
    && yum clean all

COPY python-api.py /python_api/python-api.py

ENTRYPOINT ["python3.7", "/python_api/python-api.py"]

